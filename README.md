# 数据仓库

数模校赛需要的一些数据

`COVID-19/daily_reports` 文件夹里是中国、美国、英国、印度、巴西和意大利的每日疫情报导数据

数据表表头说明

- Country_Region 国家和地区名
- Date 日期
- Confirmed 累计确诊人数
- Deaths 累计死亡人数
- Recovered 累计治愈人数

PS：

`COVID-19/daily_reports/daily_China.csv` 文件中的前几行数据有点问题，建议舍弃

`COVID-19/daily_reports/daily_US.csv` 文件里的治愈人数自 2020.12.15 日缺失，待补充



TODO:

