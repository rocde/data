# 数据来源

汇总数据源：

- World Health Organization (WHO): https://www.who.int/
- European Centre for Disease Prevention and Control (ECDC): https://www.ecdc.europa.eu/en/geographical-distribution-2019-ncov-cases
- DXY.cn. Pneumonia. 2020. http://3g.dxy.cn/newh5/view/pneumonia
- US CDC: https://www.cdc.gov/coronavirus/2019-ncov/index.html
- BNO News: https://bnonews.com/index.php/2020/02/the-latest-coronavirus-cases/
- WorldoMeters: https://www.worldometers.info/coronavirus/
- 1Point3Arces: https://coronavirus.1point3acres.com/en
- COVID Tracking Project: https://covidtracking.com/data. (US Testing and Hospitalization Data. We use the maximum reported value from "Currently" and "Cumulative" Hospitalized for our hospitalization number reported for each state.)
- Los Angeles Times: https://www.latimes.com/projects/california-coronavirus-cases-tracking-outbreak/
- The Mercury News: https://www.mercurynews.com/tag/coronavirus/

China:

- National Health Commission of the People’s Republic of China (NHC): http://www.nhc.gov.cn/xcs/yqtb/list_gzbd.shtml
- China CDC (CCDC): http://weekly.chinacdc.cn/news/TrackingtheEpidemic.htm

US:

TODO

UK:

- The UK Government: https://coronavirus.data.gov.uk/#category=nations&map=rate

India:

- Government of India: https://www.mygov.in/covid-19

Brazil:

- Brazil Ministry of Health: https://covid.saude.gov.br/
- Brazil: https://github.com/wcota/covid19br. Data described in [DOI: 10.1590/SciELOPreprints.362](https://doi.org/10.1590/SciELOPreprints.362)

Italy:

- Italy Ministry of Health: http://www.salute.gov.it/nuovocoronavirus
- Dati COVID-19 Italia (Italy): https://github.com/pcm-dpc/COVID-19/tree/master/dati-regioni

Spain:

- rtve (Spain): https://www.rtve.es/noticias/20200514/mapa-del-coronavirus-espana/2004681.shtml

Japan:

- Japan COVID-19 Coronavirus Tracker: https://covid19japan.com/#all-prefectures

Korea:

TODO

Germany:

- Berliner Morgenpost (Germany): https://interaktiv.morgenpost.de/corona-virus-karte-infektionen-deutschland-weltweit/

Bhutan:

TODO

